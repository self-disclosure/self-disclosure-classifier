import pandas as pd
import string
import math
import numpy as np
import csv


def load_file(filename):
    df = pd.read_csv(filename)
    df = df[(df["self-disclose level"].notnull()) & (df["text"].notnull())].copy()
    df = df.dropna(subset=["text"])
    return df


def load_files():
    df_1 = load_file("./data/annotated_data/movie_recommendation_annotation_kaihui.csv")
    df_2 = load_file("./data/annotated_data/movie_recommendation_annotation_weiyan.csv")
    final_df = pd.concat([df_1, df_2])
    final_df = final_df.reset_index(drop=True)
    return final_df


def get_last_partner_response(df, i):
    row = df.iloc[i]
    dialog_id = row["dialog_id"]
    speaker = row["speaker"]

    partner = "SEEKER" if speaker == "RECOMMENDER" else "RECOMMENDER"
    current_turn_id = row["turn_id"]
    last_partner_response_turn_id = current_turn_id - 1 if speaker == "RECOMMENDER" else current_turn_id

    if last_partner_response_turn_id == 0:
        result = None
    else:
        last_partner_responses = df[(df["dialog_id"] == dialog_id) &
                                    (df["turn_id"] == last_partner_response_turn_id) &
                                    (df["speaker"] == partner)]

        last_partner_responses = last_partner_responses["text"].tolist()

        if not last_partner_responses:
            result = None
        else:
            for id, response in enumerate(last_partner_responses):
                if response.strip()[-1] not in string.punctuation:
                    last_partner_responses[id] = response.strip() + "."

            result = " ".join(last_partner_responses)
            result = result.replace(":", " ")
    return result

def get_current_speaker_prev_response_at_same_turn(df, i):
    row = df.iloc[i]
    dialog_id = row["dialog_id"]
    speaker = row["speaker"]

    current_turn_id = row["turn_id"]
    current_utt_id = row["utt_id"]

    prev_responses = df[(df["dialog_id"] == dialog_id) &
                            (df["turn_id"] == current_turn_id) &
                            (df["utt_id"] < current_utt_id) &
                            (df["speaker"] == speaker)]

    prev_responses = prev_responses["text"].tolist() if not prev_responses.empty else []
    prev_responses = " ".join(prev_responses) if prev_responses else "EMPTY"
    prev_responses = prev_responses.replace(":", " ")

    return prev_responses


def preprocess(df):
    df.loc[(df["self-disclose level"] == "none"), "self-disclose level"] = "factual"
    # df = df[(df["self-disclose level"] != "factual") & (df["self-disclose level"] != "none")].copy()
    df = df[df["self-disclose level"].isin(["factual", "cognitive", "emotional"])].copy()
    df = df.dropna(subset=["text"])

    df["text"] = df["text"].str.replace('>', "?")

    for i in range(2, len(df)):
        df.at[i, "last_partner_response"] = get_last_partner_response(df, i)

        df.at[i, "speaker_prev_response"] = get_current_speaker_prev_response_at_same_turn(df, i)
        df["speaker_prev_response"] = df["speaker_prev_response"].fillna("")

        # print("df.loc[i, 'last_partner_response']", df.iloc[i, "last_partner_response"])
    df = df.dropna(subset=["last_partner_response", "text"])
    return df


def create_training_data_content(row):
    print ("last_partner_response", row["last_partner_response"])
    print("spekaer_prev_response", row["speaker_prev_response"])
    print("text", row["text"], "type", type(row["text"]))

    #
    # result = row["last_partner_response"] + " : " + \
    #          row["speaker_prev_response"] + " > " + \
    #          row["text"] + " ## " + \
    #          row["self-disclose level"] + ";"

    result = row["last_partner_response"] + " : " + \
             "EMPTY" + " > " + \
             row["text"] + " ## " + \
             row["self-disclose level"] + ";"
    result.replace('"', '')
    return result

def create_inference_data_content(row):
    print ("last_partner_response", row["last_partner_response"])
    print("spekaer_prev_response", row["speaker_prev_response"])
    print("text", row["text"], "type", type(row["text"]))

    # result = row["last_partner_response"] + " : " + \
    #          row["speaker_prev_response"] + " > " + \
    #          row["text"]

    result = row["last_partner_response"] + " : " + \
             "EMPTY" + " > " + \
             row["text"]

    result.replace('"', '')
    return result


def generate_training_data(df):
    df["training_data"] = df.apply(create_training_data_content, axis=1)
    df["inference_data"] = df.apply(create_inference_data_content, axis=1)

    print("length of training data: ", len(df))
    new_df = pd.concat([df[:510], df[580:]])
    training_data = new_df.sample(frac=0.75)
    print("training data length: ", len(training_data))

    test_set = new_df.drop(training_data.index)

    print("value counts: ")
    print(training_data['self-disclose level'].value_counts())

    max_size = training_data['self-disclose level'].value_counts().max()
    lst = [training_data]
    for class_index, group in training_data.groupby('self-disclose level'):
        lst.append(group.sample(max_size - len(group), replace=True))
    training_data = pd.concat(lst)
    print("final training data length: ", len(training_data))

    print("Final value counts: ")
    print(training_data['self-disclose level'].value_counts())

    return training_data["training_data"].tolist(), test_set["training_data"].tolist(),  test_set["inference_data"].tolist()


def output_to_txt(list, filename):
    with open(filename, 'w') as f:
        for item in list:
            f.write("%s\n" % item)


if __name__ == '__main__':
    df = load_files()
    df = preprocess(df)
    train, dev, inference = generate_training_data(df)

    output_to_txt(train, "train.txt")
    output_to_txt(dev, "dev.txt")
    output_to_txt(inference, "inference.txt")

