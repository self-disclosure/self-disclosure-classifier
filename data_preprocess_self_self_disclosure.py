import pandas as pd
import string
import math
import numpy as np
import csv
import re

def load_file(filename):
    df = pd.read_csv(filename, encoding="ISO-8859-1")
    df = df[(df["self-disclosure level"].notnull()) & (df["text"].notnull())].copy()
    df = df.dropna(subset=["text"])
    return df


def load_files():
    df_2 = load_file("./data/annotated_data/self-disclosure-user-study-dialog-annotation-v3.csv")
    # final_df = pd.concat([df_1, df_2])
    final_df = df_2.reset_index(drop=True)
    return final_df


def get_last_partner_response(df, i):
    row = df.iloc[i]
    result = row["last_response"]

    if isinstance(result, str):
        result = re.sub(r"\<([^\<])*\>", "", result)
        result = result.replace(":", " ")


    return result

def get_current_speaker_prev_response_at_same_turn(df, i):
    row = df.iloc[i]

    prev_responses = row["prev_text"]
    print("prev_responses, ", prev_responses)
    if isinstance(prev_responses, str):
        print("Type, ", type(prev_responses))
        prev_responses = prev_responses.replace(":", " ")
    else:
        print("Type not str, ", type(prev_responses))
        prev_responses = "EMPTY"

    print("Final Type, ", type(prev_responses))

    return prev_responses


def preprocess(df):
    df = df[df["self-disclosure level"].isin(["none", "factual", "cognitive", "emotional"])].copy()
    df = df.dropna(subset=["text"])

    df["text"] = df["text"].str.replace('>', "?")

    for i in range(2, len(df)):
        df.at[i, "last_partner_response"] = get_last_partner_response(df, i)
        df.at[i, "speaker_prev_response"] = get_current_speaker_prev_response_at_same_turn(df, i)
        df["speaker_prev_response"] = df["speaker_prev_response"].fillna("")

        # print("df.loc[i, 'last_partner_response']", df.iloc[i, "last_partner_response"])
    df = df.dropna(subset=["last_partner_response", "text"])
    return df


def create_training_data_content(row):
    print ("last_partner_response", row["last_partner_response"])
    print("speaker_prev_response", row["speaker_prev_response"])
    print("text", row["text"], "type", type(row["text"]))

    #
    # result = row["last_partner_response"] + " : " + \
    #          row["speaker_prev_response"] + " > " + \
    #          row["text"] + " ## " + \
    #          row["self-disclosure level"] + ";"

    result = row["last_partner_response"] + " : " + \
             row["speaker_prev_response"] + " > " + \
             row["text"] + " ## " + \
             row["self-disclosure level"] + ";"
    result.replace('"', '')
    return result

def create_inference_data_content(row):
    print ("last_partner_response", row["last_partner_response"])
    print("spekaer_prev_response", row["speaker_prev_response"])
    print("text", row["text"], "type", type(row["text"]))

    # result = row["last_partner_response"] + " : " + \
    #          row["speaker_prev_response"] + " > " + \
    #          row["text"]

    result = row["last_partner_response"] + " : " + \
             row["speaker_prev_response"] + " > " + \
             row["text"]

    result.replace('"', '')
    return result


def generate_training_data(df):
    df["training_data"] = df.apply(create_training_data_content, axis=1)
    df["inference_data"] = df.apply(create_inference_data_content, axis=1)

    print("length of training data: ", len(df))
    # new_df = pd.concat([df[:500]])
    new_df = df

    training_data = new_df.sample(frac=0.75)
    print("training data length: ", len(training_data))

    test_set = new_df.drop(training_data.index)

    print("value counts: ")
    print(training_data['self-disclosure level'].value_counts())

    max_size = training_data['self-disclosure level'].value_counts().max()
    lst = [training_data]
    for class_index, group in training_data.groupby('self-disclosure level'):
        lst.append(group.sample(max_size - len(group), replace=True))
    training_data = pd.concat(lst)
    print("final training data length: ", len(training_data))

    print("Final value counts: ")
    print(training_data['self-disclosure level'].value_counts())

    return training_data["training_data"].tolist(), test_set["training_data"].tolist(), test_set["inference_data"].tolist()


def output_to_txt(list, filename):
    with open(filename, 'w') as f:
        for item in list:
            f.write("%s\n" % item)


if __name__ == '__main__':
    df = load_files()
    df = preprocess(df)
    train, dev, inference = generate_training_data(df)

    output_to_txt(train, "train-4.txt")
    output_to_txt(dev, "dev-4.txt")
    output_to_txt(inference, "inference-4.txt")

