def analysis():
    from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
    all_predicted = [2, 2, 3, 2, 0, 2, 0, 0, 2, 0, 1, 1, 1, 2, 1, 1, 1, 2, 3, 3, 1, 3, 1, 1, 0, 2, 3, 3, 2, 1, 3, 3, 0, 1, 0, 2, 1, 2, 2, 2, 1, 1, 2, 1, 2, 2, 2, 2, 1, 2, 1, 2, 1, 1, 1, 1, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 1, 1, 1, 1, 0, 1, 2, 0, 1, 2, 2, 3, 1, 2, 1, 1, 2, 2, 1, 3, 2, 1, 0, 1, 0, 1, 2, 0, 0, 1, 2, 1, 0, 1, 1, 0, 2, 2, 2, 2, 2, 1, 1, 1, 3, 2, 1, 3, 1, 1, 2, 2, 2, 3, 0, 2, 1, 2, 3, 1, 1, 1, 1, 1, 2, 1, 1, 3, 1, 1, 1, 2, 2, 3, 3, 1, 0, 1, 1, 2, 1]
    all_actual = [1, 2, 1, 2, 0, 2, 0, 0, 2, 0, 1, 1, 1, 2, 1, 1, 1, 2, 3, 3, 2, 3, 1, 1, 0, 1, 2, 3, 2, 1, 3, 1, 0, 1, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 2, 1, 2, 1, 1, 1, 2, 2, 1, 1, 2, 2, 1, 2, 1, 1, 2, 1, 3, 1, 2, 2, 1, 1, 1, 1, 1, 0, 1, 2, 0, 1, 1, 2, 3, 1, 2, 2, 3, 2, 2, 1, 2, 2, 2, 0, 1, 0, 0, 3, 0, 0, 2, 2, 1, 1, 1, 1, 0, 1, 2, 1, 3, 2, 1, 2, 1, 3, 2, 1, 3, 1, 1, 2, 3, 1, 3, 0, 3, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1, 3, 1, 1, 1, 2, 3, 3, 3, 1, 1, 1, 2, 2, 1]
    confusion_matrix(all_actual, all_predicted)
    f1_weighted = f1_score(all_actual, all_predicted, average='weighted')
    precision = precision_score(all_actual, all_predicted, average='weighted')
    recall = recall_score(all_actual, all_predicted, average='weighted')

    print("f1", f1_weighted)
    print("precision", precision)
    print("recall", recall)


if __name__ == '__main__':
    analysis()

# confusion_matrix:
    # [[14  1  0  0]
    #  [ 2 54 12  0]
    #  [ 1  9 38  3]
    # [ 1  2  6 10]]

# confusion_matrix:
#  [[14  1  0  0]
#  [ 3 54  9  2]
#  [ 0 11 37  3]
#  [ 0  1  6 12]]